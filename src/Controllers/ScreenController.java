package Controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class ScreenController {

    private Parent root;
    private Stage stage;

    public void switchToSceneStart(ActionEvent event) throws IOException{
        root = FXMLLoader.load(getClass().getResource("/Views/StartMenu.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(new Scene(root, 800, 600));
        stage.show();
    }






}
