package Controllers;

import com.sun.tools.javac.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;

public class LoginController {

    @FXML
    private TextField username;

    @FXML
    private Button btnLogin;

    @FXML
    private PasswordField password;

    @FXML
    void onLoginClicked(ActionEvent event) throws IOException {
        ScreenController sc = new ScreenController();
        sc.switchToSceneStart(event);

    }



}
